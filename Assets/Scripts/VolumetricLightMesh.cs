﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(Light))]
public class VolumetricLightMesh : MonoBehaviour
{
    MeshFilter _filter;
    Light _light;
    Mesh _mesh;

    public float maxOpacity = 0.25f;

    void Start()
    {
        _filter = GetComponent<MeshFilter>();
        _light = GetComponent<Light>();

    }

    // Update is called once per frame
    void Update()
    {
        _mesh = BuildMesh();
        _filter.mesh = _mesh;
    }

    private Mesh BuildMesh()
    {
        _mesh = new Mesh();
        float farPosition = Mathf.Tan(_light.spotAngle*0.5f * Mathf.Deg2Rad) * _light.range;
        _mesh.vertices = new Vector3[]
        {
            new Vector3(0,0,0),
            new Vector3(farPosition, farPosition, _light.range),
            new Vector3(-farPosition, farPosition, _light.range),
            new Vector3(-farPosition, -farPosition, _light.range),
            new Vector3(farPosition, -farPosition, _light.range)
        };

        _mesh.colors = new Color[]
        {
            new Color(_light.color.r, _light.color.g, _light.color.b, _light.color.a * maxOpacity),
            new Color(_light.color.r, _light.color.g, _light.color.b, 0),
            new Color(_light.color.r, _light.color.g, _light.color.b, 0),
            new Color(_light.color.r, _light.color.g, _light.color.b, 0),
            new Color(_light.color.r, _light.color.g, _light.color.b, 0)
        };

        _mesh.triangles = new int[]
        {
            0,1,2,
            0,2,3,
            0,3,4,
            0,4,1
        };

        return _mesh;
    }
}
