﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseCursor : MonoBehaviour
{
    public Image cursorUI;
    public GameObject clickEffect;
    public GameObject trailEffect;
    public float timeBtwSpawn = 3.5f;
    
    private void Start()
    {
        Cursor.visible = false;
    }
    private void Update()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Input.GetMouseButtonDown(0))
        {



            if (Physics.Raycast(ray, out hit, 100f))
            {
                if (hit.transform != null)
                {
                    Instantiate(clickEffect, hit.point, Quaternion.identity);


                    if (hit.transform.gameObject.tag == "Size")
                    {
                        hit.transform.gameObject.GetComponent<Animator>().SetBool("activated", true);
                    }
                }
                
            }
        }

        if (Physics.Raycast(ray, out hit, 100f))
        {
            if (hit.transform != null)
            {
                if (timeBtwSpawn <= 0)
                {                    
                    Instantiate(trailEffect, hit.point, Quaternion.identity);
                    timeBtwSpawn = 0.1f;
                }
                else
                {
                    timeBtwSpawn -= Time.deltaTime;
                }

            }

        }
       
    }
}
