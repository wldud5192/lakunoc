﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurningOnSmoke : MonoBehaviour
{
    public GameObject smoke;

    public void TurnOnSmoke()
    {
        smoke.SetActive(true);
    }
}
