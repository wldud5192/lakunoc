﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SoundEffect")]
public class SoundEffectList : ScriptableObject
{
    public List<AudioClip> SoundEffect;
}
