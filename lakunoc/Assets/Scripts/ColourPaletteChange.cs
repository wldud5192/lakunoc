﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class ColourPaletteChange : MonoBehaviour
{

    public PostProcessVolume postProcess;
    ColorGrading colorGradingLayer = null;
    // Start is called before the first frame update
    void Start()
    {
        postProcess.profile.TryGetSettings(out colorGradingLayer);


        colorGradingLayer.enabled.value = true;
    }

    public void ChangeColour()
    {

        postProcess.profile.TryGetSettings(out colorGradingLayer);

        for (int i = 0; i > 50; i++)
        {
            colorGradingLayer.hueShift.value++;
        }
    }
}
