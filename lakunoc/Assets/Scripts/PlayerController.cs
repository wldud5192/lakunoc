﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public float degreesPerSecond = 10f;
    public float amplitude = 0.5f;
    public float frequency = 0.5f;

    float speed;
    float yAxis;
    Vector3 posOffset = new Vector3();
    Vector3 tempPos = new Vector3();


    // Start is called before the first frame update
    void Start()
    {
        posOffset = transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        //GetComponent<Rigidbody>().MovePosition(yMovement);
    }

    private void LateUpdate()
    {
        yAxis = Input.GetAxis("Vertical");
        if (yAxis != 0)
        {
          //  speed = Mathf.Lerp(0.5f, 2f, 3f);
        }
        Vector3 yMovement = (yAxis * transform.up) * speed;
        transform.localPosition = posOffset + yMovement;

        if (yAxis < 1)
        {
                
            transform.Rotate(new Vector3(0f, Time.deltaTime * degreesPerSecond, 0f), Space.World);
            tempPos = transform.localPosition;
            tempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;
            transform.localPosition = tempPos;
        }
    }
}
