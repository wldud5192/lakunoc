﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace UnityStandardAssets.Characters.FirstPerson
{
        public class PlayerCamera : MonoBehaviour
    {
        [SerializeField] private MouseLook m_MouseLook;

        Camera m_Camera;
        GameObject postProcess;

        ColorGrading colourGradingLayer = null;

        bool changeColourPalette;

        // Use this for initialization
        private void Start()
        {
            postProcess = GameObject.FindGameObjectWithTag("PostProcessing");
            m_Camera = Camera.main;
            m_MouseLook.Init(transform, m_Camera.transform);
           // m_MouseLook.SetCursorLock(false);
        }


        // Update is called once per frame
        private void Update()
        {
            RotateView();
            if (changeColourPalette)
            {
                colourGradingLayer.saturation.value = Mathf.Lerp(-25, 25f, 30);
            }

        }

        private void FixedUpdate()
        {
            m_MouseLook.UpdateCursorLock();
        }


        private void RotateView()
        {
            m_MouseLook.LookRotation(transform, m_Camera.transform);
        }

        void ColourPaletteChange()
        {
            PostProcessVolume volume = postProcess.GetComponent<PostProcessVolume>();
            volume.profile.TryGetSettings(out colourGradingLayer);
            changeColourPalette = true;

        }
    }
        
    
}
