﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class MainMenuBackground : MonoBehaviour
{
    public Texture2D cursorTexture;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
    
    Animator credit;
    public List<GameObject> Background;
    public List<Material> Skyboxs;
    public int backgroundNum;
    public GameObject sceneSelection;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        backgroundNum = Random.Range(0, 3);
        if (backgroundNum == 0)
        {
            LoadBackground0();
        }
        else if (backgroundNum == 1)
        {
            LoadBackground1();
        }
        else
        {
            LoadBackground2();
        }
        credit = GameObject.FindGameObjectWithTag("Credits").GetComponent<Animator>();
    }
    
    public void LoadBackground0()
    {
        Background[0].SetActive(true);
        RenderSettings.skybox = Skyboxs[0];

    }
    public void LoadBackground1()
    {
        Background[1].SetActive(true);
        RenderSettings.skybox = Skyboxs[1];
    }
    public void LoadBackground2()
    {
        Background[2].SetActive(true);
        RenderSettings.skybox = Skyboxs[2];
    }

    public void Credits()
    {
        if (!credit.GetBool("creditIsPlaying"))
        {
            credit.SetBool("creditIsPlaying", true);
        } else
        {
            credit.SetBool("creditIsPlaying", false);
        }
    }
    
    public void LoadSceneSelect()
    {
        sceneSelection.SetActive(true);
    }
}
