﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering.PostProcessing;

public class MouseCursor : MonoBehaviour
{
    public Image cursorUI;
    public GameObject clickEffect;
    public GameObject trailEffect;
    public Animator jellyFishColourChange;
    public GameObject mushroomLightEffect;
    public GameObject jellyFishEffect;
    public float timeBtwSpawn = 3.5f;
    public Material rockStrat;
    public List<Material> RocksColour;
    public float colourChangeTime;
    public List<SoundEffectList> soundEffectLists;

    public Material[] sharkMaterial;
    public GameObject sharkCloud;
    AudioSource soundEffect;

    void Start()
    {
        soundEffect = GetComponent<AudioSource>();
        Cursor.visible = false;
    }
    private void Update()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Input.GetMouseButtonDown(0))
        {



            if (Physics.Raycast(ray, out hit, 10000f))
            {
                if (hit.transform != null)
                {
                    Instantiate(clickEffect, hit.point, Quaternion.identity);
                                      
                    
                    if (hit.transform.gameObject.tag == "Size")
                    {
                        int ranSound = Random.Range(0, 3);
                        soundEffect.PlayOneShot(soundEffectLists[0].SoundEffect[ranSound]);
                        hit.transform.gameObject.GetComponent<Animator>().SetBool("activated", true);
                    }

                    if (hit.transform.gameObject.tag == "Mushroom")
                    {
                        int ranSound = Random.Range(0, 3);
                        soundEffect.PlayOneShot(soundEffectLists[1].SoundEffect[ranSound]);
                        Instantiate(mushroomLightEffect, hit.transform.gameObject.transform.position, Quaternion.identity);
                    }

                    if (hit.transform.gameObject.tag == "Seaweed")
                    {
                        int ranSound = Random.Range(0, 3);
                        soundEffect.PlayOneShot(soundEffectLists[2].SoundEffect[ranSound]);
                        hit.transform.gameObject.GetComponentInChildren<ParticleSystem>().Play();
                    }

                    if (hit.transform.gameObject.tag == "JellyFish")
                    {
                        Debug.Log(".");
                        int ranSound = Random.Range(0, 1);
                        soundEffect.PlayOneShot(soundEffectLists[3].SoundEffect[ranSound]);

                        hit.transform.gameObject.GetComponentInChildren<ParticleSystem>().Play();
                        jellyFishColourChange.SetBool("activated", true);
                    }
                    if (hit.transform.gameObject.tag == "Tree")
                    {
                        var ranTreeSize = Random.Range(0, 3);
                        int ranSound = Random.Range(0, 4);
                        soundEffect.PlayOneShot(soundEffectLists[4].SoundEffect[ranSound]);
                        if (ranTreeSize == 0)
                        {
                            hit.transform.gameObject.GetComponent<Animator>().SetBool("isBig", true);
                        }
                        else if (ranTreeSize == 1)
                        {
                            hit.transform.gameObject.GetComponent<Animator>().SetBool("isSmall", true);
                        }
                        else
                        {
                            hit.transform.gameObject.GetComponent<Animator>().SetBool("isThin", true);
                        }
                    }
                    if (hit.transform.gameObject.tag == "Rock")
                    {
                        var ranColour = Random.Range(0, 3);
                        int ranSound = Random.Range(0, 3);
                        soundEffect.PlayOneShot(soundEffectLists[5].SoundEffect[ranSound]);
                        if (ranColour == 0)
                        {
                            hit.transform.gameObject.GetComponent<Renderer>().material.Lerp(rockStrat, RocksColour[ranColour], colourChangeTime);
                        }
                        else if (ranColour == 1)
                        {
                            hit.transform.gameObject.GetComponent<Renderer>().material.Lerp(rockStrat, RocksColour[ranColour], colourChangeTime);
                        }
                        else
                        {
                            hit.transform.gameObject.GetComponent<Renderer>().material.Lerp(rockStrat, RocksColour[ranColour], colourChangeTime);
                        }
                    }


                    if (hit.transform.gameObject.tag == "Cloud")
                    {
                        var ranCloudSize = Random.Range(0, 3);
                        int ranSound = Random.Range(0, 3);
                        soundEffect.PlayOneShot(soundEffectLists[6].SoundEffect[ranSound]);
                        if (ranCloudSize == 0)
                        {
                            hit.transform.gameObject.GetComponent<Animator>().SetBool("isBig", true);
                        }
                        else if (ranCloudSize == 1)
                        {
                            hit.transform.gameObject.GetComponent<Animator>().SetBool("isSmall", true);
                        }
                        else
                        {
                            hit.transform.gameObject.GetComponent<Animator>().SetBool("isWide", true);
                        }
                    }

                    if (hit.transform.gameObject.tag == "Star")
                    {
                        int ranSound = Random.Range(0, 3);
                        soundEffect.PlayOneShot(soundEffectLists[7].SoundEffect[ranSound]);
                        hit.transform.gameObject.GetComponentInChildren<ParticleSystem>().Play();
                    }

                    if (hit.transform.gameObject.tag == "Shark")
                    {
                        int ranSound = Random.Range(0, 3);
                        soundEffect.PlayOneShot(soundEffectLists[8].SoundEffect[ranSound]);

                        Animator cameraAnim = Camera.main.GetComponent<Animator>();
                        cameraAnim.SetBool("cameraTint", true);

                        Debug.Log(".");
                        int ranMat = Random.Range(0, 3);
                        switch (ranMat)
                        {
                            case 0:
                                sharkCloud.GetComponent<Renderer>().material = sharkMaterial[0];
                                break;
                            case 1:
                                sharkCloud.GetComponent<Renderer>().material = sharkMaterial[1];
                                break;
                            case 2:
                                sharkCloud.GetComponent<Renderer>().material = sharkMaterial[2];
                                break;
                            case 3:
                                sharkCloud.GetComponent<Renderer>().material = sharkMaterial[3];
                                break;
                        }
                    }
                    
                    if (hit.transform.gameObject.tag == "Untagged")
                    {
                        int ranSound = Random.Range(0, 3);
                        soundEffect.PlayOneShot(soundEffectLists[9].SoundEffect[ranSound]);
                    }

                }

            }
        }

        if (Physics.Raycast(ray, out hit, 100f))
        {
            if (hit.transform != null)
            {
                if (timeBtwSpawn <= 0)
                {
                    Instantiate(trailEffect, hit.point, Quaternion.identity);
                    timeBtwSpawn = 0.1f;
                }
                else
                {
                    timeBtwSpawn -= Time.deltaTime;
                }

            }

        }

    }
}
