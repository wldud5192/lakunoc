﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Gate : MonoBehaviour
{

    public Transform player;
    public GameObject spawnEffect;
    public int sceneIndex;
    public Animator fadeOut;
    public AudioSource fadeOutSound;

    bool spawn = false;

    private void Update()
    {
        if (!spawn)
        {
            StartCoroutine(SpawnEffect());
        }
    }

    IEnumerator SpawnEffect()
    {
        spawn = true;
        Instantiate(spawnEffect, player.position, Quaternion.LookRotation(Camera.main.ScreenPointToRay(Input.mousePosition).direction));
        yield return new WaitForSecondsRealtime(4f);
        spawn = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            StartCoroutine(SceneChange());
        }
    }

    IEnumerator SceneChange()
    {
        Debug.Log("SceneChange");
        fadeOut.enabled = true;
        //fadeOutSound.Play();
        yield return new WaitForSecondsRealtime(4);
        SceneManager.LoadScene(sceneIndex);
    }
}
