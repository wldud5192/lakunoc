﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MountainGate : MonoBehaviour
{

    public Image fadeOut;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            StartCoroutine(SceneChange());
        }
    }

    IEnumerator SceneChange()
    {
        fadeOut.enabled = true;
        //  fadeOutSound.Play();
        yield return new WaitForSecondsRealtime(4);
        SceneManager.LoadScene(1);
    }

}
