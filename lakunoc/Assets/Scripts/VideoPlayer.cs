﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class VideoPlayer : MonoBehaviour
{
    public UnityEngine.Video.VideoClip videoClip;
    public Camera mainCamera;

    // Start is called before the first frame update
    void Start()
    {
        var videoPlayer = gameObject.AddComponent<UnityEngine.Video.VideoPlayer>();
        var audioSource = gameObject.AddComponent<AudioSource>();
        videoPlayer.clip = videoClip;
        videoPlayer.targetCamera = mainCamera;
        videoPlayer.renderMode = UnityEngine.Video.VideoRenderMode.CameraNearPlane;
        videoPlayer.targetMaterialRenderer = GetComponent<Renderer>();
        videoPlayer.targetMaterialProperty = "_MainTex";
        videoPlayer.audioOutputMode = UnityEngine.Video.VideoAudioOutputMode.AudioSource;
        videoPlayer.SetTargetAudioSource(0, audioSource);
        StartCoroutine(RandomBreathingScene());
    }

    IEnumerator RandomBreathingScene()
    {
        yield return new WaitForSecondsRealtime(33f);
        int randomScene = Random.Range(0, 2);
        switch (randomScene)
        {
            case 0:
                SceneManager.LoadScene(5);
                break;
            case 1:
                SceneManager.LoadScene(6);
                break;
            case 2:
                SceneManager.LoadScene(7);
                break;
        }

    }
}
