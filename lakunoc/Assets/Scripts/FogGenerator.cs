﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogGenerator : MonoBehaviour
{
    Transform player;
    Vector3 generationLocation;
    bool generated = false;
    public GameObject fog;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        generationLocation = player.position - transform.position;
    }

    private void Update()
    {
        if (!generated)
        {
            StartCoroutine(GenerateFog());
        }
    }

    IEnumerator GenerateFog()
    {
        generated = true;
        Instantiate(fog, player.position - generationLocation, Quaternion.identity);
        yield return new WaitForSecondsRealtime(3f);
        generated = false;
    }
}
