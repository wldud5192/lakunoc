﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarFallen : MonoBehaviour
{
    public List<Animator> Path;
    public int pathNum;
    public AudioSource audioSource;
    public AudioClip soundEffect;
    public void StarFalls()
    {
        Path[pathNum].SetBool("isOpen", true);
        audioSource.PlayOneShot(soundEffect);
        pathNum += 1;
    }
    public void StarunFall()
    {
        pathNum -= 1;
        Path[pathNum].SetBool("isOpen", false);
        pathNum += 1;
    }

}
