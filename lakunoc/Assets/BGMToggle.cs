﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BGMToggle : MonoBehaviour
{
    Image screenBrightnessImage;
    public void BgmChange()
    {
        if (GetComponent<Toggle>().isOn)
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<AudioSource>().enabled = true;
        }
        if (GetComponent<Toggle>().isOn == false)

        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<AudioSource>().enabled = false;
        }
    }

    public void WhiteNoiseChange()
    {

        if (GetComponent<Toggle>().isOn)
        {
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AudioSource>().enabled = true;
        }

        if (GetComponent<Toggle>().isOn == false)
        {
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<AudioSource>().enabled = false;
        }
    }

    public void ScreenBrightness()
    {
        if (GetComponent<Slider>().value > 0)
        {
            GameObject.FindGameObjectWithTag("Screen").GetComponent<Image>().enabled = true;
        } else
        {
            GameObject.FindGameObjectWithTag("Screen").GetComponent<Image>().enabled = false;
        }
        screenBrightnessImage = GameObject.FindGameObjectWithTag("Screen").GetComponent<Image>();
        var brightness = screenBrightnessImage.color;
        brightness.a = GetComponent<Slider>().value;
        screenBrightnessImage.color = brightness;
    }

    public void TimerSlider()
    {
        float timerValue = GameObject.FindGameObjectWithTag("Timer").GetComponent<Slider>().value;
        switch (timerValue)
        {
            case 0:
                Camera.main.GetComponent<Animator>().Play("Breathing_3min");
                break;
            case 1:
                Camera.main.GetComponent<Animator>().Play("Breathing_15min");
                break;
            case 2:
                Camera.main.GetComponent<Animator>().Play("Breathing_30min");
                break;
            case 3:
                Camera.main.GetComponent<Animator>().Play("Breathing_60min");
                break;
            case 4:
                Camera.main.GetComponent<Animator>().Play("Breathing_1440min");
                break;
        }
    }
}
