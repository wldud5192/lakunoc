﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CustomPauseMenu : MonoBehaviour
{
    static bool GamePaused = false;
    public GameObject pauseComponent;
    
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GamePaused)
            {
                Resume();
            }

            else
            {
                Pause();
            }
            
        }
    }

    public void Resume()
    {

        AudioListener.pause = false;
        pauseComponent.SetActive(false);
        Time.timeScale = 1f;
        GamePaused = false;
    }

    void Pause ()
    {
        AudioListener.pause = true;
        pauseComponent.SetActive(true);
        Time.timeScale = 0f;
        GamePaused = true;
    }

    public void ChangeScene(int index)
    {
        StartCoroutine(ChangeSceneEnum(index));
    }

    IEnumerator ChangeSceneEnum(int sceneIndex)
    {
        Debug.Log("SceneChange");
        Time.timeScale = 1f;
        AudioListener.pause = false;
        Animator fadeOut = GameObject.FindGameObjectWithTag("FadeOut").GetComponent<Animator>();
        fadeOut.enabled = true;
        yield return new WaitForSecondsRealtime(4);
        SceneManager.LoadScene(sceneIndex);
    }
}
