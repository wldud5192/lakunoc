﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreathingCustomization : MonoBehaviour
{
    static bool GamePaused = false;
    public GameObject breathingOption;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            
            if (GamePaused)
            {
                Resume();
            }

            else
            {
                Pause();
            }

        }
    }

    public void Resume()
    {

        AudioListener.pause = false;
        breathingOption.SetActive(false);
        Time.timeScale = 1f;
        GamePaused = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Pause()
    {
        AudioListener.pause = true;
        breathingOption.SetActive(true);
        Time.timeScale = 0f;
        GamePaused = true;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
}
