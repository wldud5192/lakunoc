using System;
using UnityEngine;

public class MenuSceneLoader : MonoBehaviour
{
    public GameObject menuUI;

    private GameObject m_Go;

	void Awake ()
	{
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        if (m_Go == null)
	    {
	        m_Go = Instantiate(menuUI);
	    }
	}
}
