﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BreathingInstruction : MonoBehaviour
{
    [TextArea]
    public string[] line;
    int i = 1;

   void ChangeText()
    {
        GetComponent<Text>().text = line[i];
        i++;
    }
}
